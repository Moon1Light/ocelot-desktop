package ocelot.desktop

import java.io._
import java.nio.file.{Files, Path}
import java.util.concurrent.locks.{Lock, ReentrantLock}

import javax.swing.{JFileChooser, JOptionPane}
import li.flor.nativejfilechooser.NativeJFileChooser
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.widget.RootWidget
import ocelot.desktop.util._
import org.apache.commons.io.FileUtils
import org.apache.logging.log4j.LogManager
import totoro.ocelot.brain.Ocelot
import totoro.ocelot.brain.event._
import totoro.ocelot.brain.nbt.{CompressedStreamTools, NBTTagCompound}
import totoro.ocelot.brain.workspace.Workspace

import scala.io.Source
import scala.jdk.CollectionConverters._

object OcelotDesktop extends Logging {
  var root: RootWidget = _
  val tpsCounter = new FPSCalculator
  val tickLock: Lock = new ReentrantLock()

  def mainInner(): Unit = {
    logger.info("Starting up Ocelot Desktop")

    Ocelot.initialize(LogManager.getLogger(Ocelot))
    ColorScheme.load(Source.fromURL(getClass.getResource("/ocelot/desktop/colorscheme.txt")))

    createWorkspace()

    root = new RootWidget()

    UiHandler.init(root)

    setupEventHandlers()

    new Thread(() => {
      val ticker = new Ticker
      ticker.tickInterval = 50
      while (true) {
        tickLock.lock()
        workspace.update()
        tpsCounter.tick()
        tickLock.unlock()
        ticker.waitNext()
      }
    }).start()

    UiHandler.start()

    logger.info("Cleaning up")

    ResourceManager.freeResources()
    UiHandler.terminate()

    logger.info("Thanks for using Ocelot Desktop")
    System.exit(0)
  }

  def main(args: Array[String]): Unit = {
    try mainInner()
    catch {
      case e: Exception =>
        val sw = new StringWriter
        val pw = new PrintWriter(sw)

        e.printStackTrace(pw)

        logger.error(s"${sw.toString}")
        System.exit(1)
    }
  }

  private def saveWorld(nbt: NBTTagCompound): Unit = {
    tickLock.lock()

    val backendNBT = new NBTTagCompound
    val frontendNBT = new NBTTagCompound
    workspace.save(backendNBT)
    root.workspaceView.save(frontendNBT)
    nbt.setTag("back", backendNBT)
    nbt.setTag("front", frontendNBT)

    tickLock.unlock()
  }

  private def loadWorld(nbt: NBTTagCompound): Unit = {
    tickLock.lock()

    val backendNBT = nbt.getCompoundTag("back")
    val frontendNBT = nbt.getCompoundTag("front")

    workspace.load(backendNBT)
    root.workspaceView.load(frontendNBT)

    tickLock.unlock()
  }

  private var savePath: Option[Path] = None
  private val tmpPath = Files.createTempDirectory("ocelot-save")

  def newWorkspace(): Unit = {
    root.workspaceView.newWorkspace()
  }

  def save(): Unit = {
    if (savePath.isEmpty) {
      saveAs()
      return
    }

    val oldPath = workspace.path
    val newPath = savePath.get
    if (oldPath != newPath) {
      val oldFiles = Files.list(oldPath).iterator.asScala.toArray
      val newFiles = Files.list(newPath).iterator.asScala.toArray
      val toRemove = newFiles.intersect(oldFiles)

      for (path <- toRemove) {
        if (Files.isDirectory(path)) {
          FileUtils.deleteDirectory(path.toFile)
        } else {
          Files.delete(path)
        }
      }

      for (path <- oldFiles) {
        FileUtils.copyDirectory(oldPath.resolve(path.getFileName).toFile, newPath.resolve(path.getFileName).toFile)
      }

      workspace.path = newPath
    }

    val path = newPath + "/workspace.nbt"
    val writer = new DataOutputStream(new FileOutputStream(path))
    val nbt = new NBTTagCompound
    saveWorld(nbt)
    CompressedStreamTools.writeCompressed(nbt, writer)
    writer.flush()
  }

  def saveAs(): Unit = {
    chooseDirectory(dir => {
      if (dir.isDefined) {
        savePath = dir.map(_.toPath)
        save()
      }
    })
  }

  def open(): Unit = {
    chooseDirectory(dir => {
      if (dir.isDefined) {
        val path = dir.get + "/workspace.nbt"
        val reader = new DataInputStream(new FileInputStream(path))
        val nbt = CompressedStreamTools.readCompressed(reader)
        savePath = Some(dir.get.toPath)
        workspace.path = dir.get.toPath
        loadWorld(nbt)
      }
    })
  }

  def chooseDirectory(fun: Option[File] => Unit): Unit = {
    new Thread(() => {
      val chooser: JFileChooser = try {
        new NativeJFileChooser
      } catch {
        case _: Throwable => new JFileChooser()
      }

      chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY)

      val option = chooser.showOpenDialog(null)
      fun(if (option == JFileChooser.APPROVE_OPTION) Some(chooser.getSelectedFile) else None)
    }).start()
  }

  def cleanup(): Unit = {
    FileUtils.deleteDirectory(tmpPath.toFile)
  }

  def exit(): Unit = {
    if (savePath.isDefined) {
      save()
      UiHandler.exit()
      cleanup()
      return
    }

    new Thread(() => {
      val dialogResult = JOptionPane.showConfirmDialog(null, "Save workspace before exiting?", "Warning", JOptionPane.YES_NO_OPTION)
      if (dialogResult == JOptionPane.YES_OPTION) {
        chooseDirectory(dir => {
          if (dir.isDefined) {
            savePath = dir.map(_.toPath)
            save()
            UiHandler.exit()
            cleanup()
          }
        })
      } else {
        UiHandler.exit()
        cleanup()
      }
    }).start()
  }

  var workspace: Workspace = _

  private def createWorkspace(): Unit = {
    workspace = new Workspace(tmpPath)
  }

  private def setupEventHandlers(): Unit = {
    EventBus.listenTo(classOf[BeepEvent], { case event: BeepEvent =>
      if (!UiHandler.audioDisabled)
        Audio.beep(event.frequency, event.duration)
      else
        logger.info(s"Beep ${event.frequency} Hz for ${event.duration} ms")
    })

    EventBus.listenTo(classOf[BeepPatternEvent], { case event: BeepPatternEvent =>
      if (!UiHandler.audioDisabled)
        Audio.beep(event.pattern)
      else
        logger.info(s"Beep pattern `${event.pattern}``")
    })

    EventBus.listenTo(classOf[MachineCrashEvent], { case event: MachineCrashEvent =>
      logger.info(s"[EVENT] Machine crash! (address = ${event.address}, ${event.message})")
    })
  }
}
