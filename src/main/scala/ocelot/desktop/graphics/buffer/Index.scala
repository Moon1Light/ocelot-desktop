package ocelot.desktop.graphics.buffer

import java.nio.ByteBuffer

case class Index(i: Int) extends BufferPut {
  override def stride: Int = 4

  override def put(buf: ByteBuffer): Unit = {
    buf.putInt(i)
  }
}
