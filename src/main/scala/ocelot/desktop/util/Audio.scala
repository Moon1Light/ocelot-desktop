package ocelot.desktop.util

import java.nio.ByteBuffer

import org.lwjgl.openal.AL10

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Audio extends Logging {
  private val sampleRate: Int = 44100
  private val amplitude: Int = 32

  private val sources = new mutable.HashMap[Int, Int]()
  private val scheduled = new ArrayBuffer[(String, Short, Short)]()

  def beep(frequency: Short, duration: Short): Unit = {
    scheduled += ((".", frequency, duration))
  }

  def beep(pattern: String): Unit = {
    scheduled += ((pattern, 1000, 200))
  }

  private def _beep(pattern: String, frequency: Short, duration: Short): Unit = {
    val source = AL10.alGenSources()
    AL10.alSourcef(source, AL10.AL_PITCH, 1)
    AL10.alSourcef(source, AL10.AL_GAIN, 0.3f)
    AL10.alSource3f(source, AL10.AL_POSITION, 0, 0, 0)
    AL10.alSourcei(source, AL10.AL_LOOPING, AL10.AL_FALSE)

    val sampleCounts = pattern.toCharArray
      .map(ch => if (ch == '.') duration else 2 * duration)
      .map(_ * sampleRate / 1000)

    val pauseSampleCount = 50 * sampleRate / 1000
    val data = ByteBuffer.allocateDirect(sampleCounts.sum + (sampleCounts.length - 1) * pauseSampleCount)
    val step = frequency / sampleRate.toFloat
    var offset = 0f

    for (sampleCount <- sampleCounts) {
      for (_ <- 0 until sampleCount) {
        val angle = 2 * math.Pi * offset
        val value = (math.signum(math.sin(angle)) * amplitude).toByte ^ 0x80
        offset += step
        if (offset > 1) offset -= 1
        data.put(value.toByte)
      }
      if (data.hasRemaining) {
        for (_ <- 0 until pauseSampleCount) {
          data.put(127: Byte)
        }
      }
    }

    data.rewind()

    val buffer = AL10.alGenBuffers()
    AL10.alBufferData(buffer, AL10.AL_FORMAT_MONO8, data, sampleRate)

    AL10.alSourceQueueBuffers(source, buffer)
    AL10.alSourcePlay(source)

    sources += (source -> buffer)
  }

  def update(): Unit = {
    for ((pattern, frequency, duration) <- scheduled)
      _beep(pattern, frequency, duration)

    scheduled.clear()

    sources.filterInPlace((source, buffer) => {
      AL10.alGetSourcei(source, AL10.AL_SOURCE_STATE) == AL10.AL_PLAYING || {
        AL10.alDeleteSources(source)
        AL10.alDeleteBuffers(buffer)
        false
      }
    })
  }
}
