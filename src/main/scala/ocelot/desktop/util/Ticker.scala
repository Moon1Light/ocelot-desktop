package ocelot.desktop.util

import java.util.concurrent.TimeUnit

class Ticker extends Logging {
  var lastTick: Long = System.nanoTime()

  private[this] var _tickInterval: Long = _

  def tickInterval: Long = _tickInterval / 1000000

  def tickInterval_=(ms: Float): Unit = {
    logger.info(f"Setting tick interval to $ms ms (${1000f / ms} s^-1)")
    _tickInterval = (ms * 1000000f).toLong
  }

  tickInterval = 50

  def waitNext(): Unit = {
    val deadline = lastTick + _tickInterval
    var time = System.nanoTime()
    while (time < deadline) {
      val rem = deadline - time
      if (rem > 1000000)
        TimeUnit.NANOSECONDS.sleep(rem - 1000000)
      else
        Thread.`yield`()
      time = System.nanoTime()
    }

    lastTick = System.nanoTime()
  }
}
