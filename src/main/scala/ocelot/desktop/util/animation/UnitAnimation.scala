package ocelot.desktop.util.animation

import ocelot.desktop.ui.UiHandler
import ocelot.desktop.util.animation.easing.Easing

object UnitAnimation {
  def linear(duration: Float) = new UnitAnimation(duration, Easing.linear)

  def easeInQuad(duration: Float) = new UnitAnimation(duration, Easing.easeInQuad)

  def easeOutQuad(duration: Float) = new UnitAnimation(duration, Easing.easeOutQuad)

  def easeInOutQuad(duration: Float) = new UnitAnimation(duration, Easing.easeInOutQuad)
}

class UnitAnimation(duration: Float, function: Float => Float) {
  var direction: Float = 1
  var time: Float = 0

  def value: Float = function(time)

  def goUp(): Unit = direction = 1

  def goDown(): Unit = direction = -1

  def reverse(): Unit = direction = -direction

  def update(): Unit = {
    time += direction * UiHandler.dt / duration
    time = math.max(0, math.min(1, time))
  }
}
