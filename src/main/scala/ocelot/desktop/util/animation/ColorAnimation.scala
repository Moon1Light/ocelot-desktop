package ocelot.desktop.util.animation

import ocelot.desktop.color.{Color, HSVAColor, RGBAColor, RGBAColorNorm}
import ocelot.desktop.util.animation.easing.EasingFunction

class ColorAnimation(init: Color, speed: Float = 6f) {
  private val initHSV = init.toHSVA
  private val h = new ValueAnimation(initHSV.h, speed * 360f)
  private val s = new ValueAnimation(initHSV.s, speed)
  private val v = new ValueAnimation(initHSV.v, speed)
  private val a = new ValueAnimation(initHSV.a, speed)

  def getHSVA: HSVAColor = HSVAColor(h.value, s.value, v.value, a.value)

  def getRGBA: RGBAColor = getHSVA.toRGBA

  def getRGBANorm: RGBAColorNorm = getHSVA.toRGBANorm

  def color: Color = getHSVA

  def easing: EasingFunction = h.easing

  def easing_=(value: EasingFunction): Unit = {
    h.easing = value
    s.easing = value
    v.easing = value
    a.easing = value
  }

  def jump(to: Color): Unit = {
    val col = to.toHSVA
    h.jump(col.h)
    s.jump(col.s)
    v.jump(col.v)
    a.jump(col.a)
  }

  def goto(to: Color): Unit = {
    val col = to.toHSVA
    h.goto(col.h)
    s.goto(col.s)
    v.goto(col.v)
    a.goto(col.a)
  }

  def update(): Unit = {
    h.update()
    s.update()
    v.update()
    a.update()
  }
}
