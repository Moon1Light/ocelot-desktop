package ocelot.desktop.util.animation.easing

object EaseInOutQuad extends EasingFunction {
  override def derivative(t: Float): Float =
    if (t < 0.5) 4 * t
    else 4 - 4 * t

  override def apply(t: Float): Float =
    if (t < 0.5) 2 * t * t
    else -1 + (4 - 2 * t) * t
}
