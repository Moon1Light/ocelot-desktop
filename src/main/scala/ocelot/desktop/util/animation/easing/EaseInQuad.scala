package ocelot.desktop.util.animation.easing

object EaseInQuad extends EasingFunction {
  override def derivative(t: Float): Float = 2 * t

  override def apply(t: Float): Float = t * t
}
