package ocelot.desktop.util

import scala.collection.mutable.ArrayBuffer

object ResourceManager {
  private val resources = new ArrayBuffer[Resource]

  def registerResource(resource: Resource): Unit = {
    resources += resource
  }

  def freeResource(resource: Resource): Unit = {
    resource.freeResource()
    resources -= resource
  }

  def freeResources(): Unit = {
    for (resource <- resources)
      resource.freeResource()

    resources.clear()
  }
}
