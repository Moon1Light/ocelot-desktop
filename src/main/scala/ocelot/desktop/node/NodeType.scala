package ocelot.desktop.node

case class NodeType(name: String, icon: String, tier: Int, factory: () => Node) extends Ordered[NodeType] {
  override def compare(that: NodeType): Int = this.name.compare(that.name)
}
