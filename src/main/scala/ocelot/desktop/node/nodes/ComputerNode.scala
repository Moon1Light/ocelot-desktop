package ocelot.desktop.node.nodes

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.color.Color
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.node.Node
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry, ContextMenuSubmenu}
import ocelot.desktop.ui.widget.slot._
import ocelot.desktop.util.TierColor
import totoro.ocelot.brain.entity.traits.{Computer, Entity}
import totoro.ocelot.brain.entity.{CPU, Case, EEPROM, FloppyManaged, GraphicsCard, HDDManaged, Memory}
import totoro.ocelot.brain.loot.Loot
import totoro.ocelot.brain.nbt.NBTTagCompound
import totoro.ocelot.brain.util.Tier

class ComputerNode(val computer: Case, setup: Boolean = true) extends Node {
  var lastFilesystemAccess = 0L

  var eepromSlot: EEPROMSlot = _
  var cpuSlot: CPUSlot = _
  var memorySlots: Array[MemorySlot] = _
  var cardSlots: Array[CardSlot] = _
  var diskSlots: Array[DiskSlot] = _
  var floppySlot: Option[FloppySlot] = None

  setupSlots()

  if (setup) {
    computer.add(new CPU(computer.tier.min(2)))
    computer.add(new Memory(computer.tier.min(2) * 2 + 1))
    computer.add(new Memory(computer.tier.min(2) * 2 + 1))
    computer.add(new GraphicsCard(computer.tier.min(1)))
    computer.add(Loot.OpenOsFloppy.create())
    computer.add(Loot.OpenOsEEPROM.create())
//    refitSlots()
    OcelotDesktop.workspace.add(computer)
  }

  refitSlots()

  def this(nbt: NBTTagCompound) {
    this({
      val address = nbt.getString("address")
      OcelotDesktop.workspace.entityByAddress(address).get.asInstanceOf[Case]
    }, setup = false)
    super.load(nbt)
  }

  override def save(nbt: NBTTagCompound): Unit = {
    super.save(nbt)
    nbt.setString("address", computer.node.address)
  }

  override def environment: Computer = computer

  override val icon: String = "nodes/Computer"
  override def iconColor: Color = TierColor.get(computer.tier)

  override def setupContextMenu(menu: ContextMenu): Unit = {
    if (computer.machine.isRunning) {
      menu.addEntry(new ContextMenuEntry("Turn off", () => computer.turnOff()))
      menu.addEntry(new ContextMenuEntry("Reboot", () => {
        computer.turnOff()
        computer.turnOn()
      }))
    } else
      menu.addEntry(new ContextMenuEntry("Turn on", () => computer.turnOn()))

    menu.addEntry(new ContextMenuSubmenu("Set tier") {
      addEntry(new ContextMenuEntry("Tier 1", () => changeTier(Tier.One)))
      addEntry(new ContextMenuEntry("Tier 2", () => changeTier(Tier.Two)))
      addEntry(new ContextMenuEntry("Tier 3", () => changeTier(Tier.Three)))
      addEntry(new ContextMenuEntry("Creative", () => changeTier(Tier.Four)))
    })

    menu.addSeparator()
    super.setupContextMenu(menu)
  }

  private def changeTier(n: Int): Unit = {
    computer.tier = n
    setupSlots()
    refitSlots()
    if (currentWindow != null) currentWindow.updateSlots()
  }

  private def refitSlots(): Unit = {
    cpuSlot._item = None
    for (slot <- memorySlots) slot._item = None
    for (slot <- cardSlots) slot._item = None
    for (slot <- diskSlots) slot._item = None

    for (item <- computer.inventory) {
      try {
        item match {
          case cpu: CPU =>
            if (cpuSlot.tier >= cpu.tier)
              cpuSlot._item = Some(cpu)
            else
              computer.remove(item)
          case mem: Memory => memorySlots
            .find(slot => slot._item.isEmpty && slot.tier >= (mem.tier + 1) / 2 - 1)
            .get._item = Some(mem)
          case hdd: HDDManaged => diskSlots
            .find(slot => slot._item.isEmpty && slot.tier >= hdd.tier)
            .get._item = Some(hdd)
          case eeprom: EEPROM => eepromSlot._item = Some(eeprom)
          case floppy: FloppyManaged => floppySlot.get._item = Some(floppy)
          case card: Entity => cardSlots
            .find(slot => slot._item.isEmpty && slot.tier >= CardRegistry.getTier(card))
            .get._item = Some(card)
          case _ =>
        }
      } catch {
        case _: NoSuchElementException => computer.remove(item)
      }
    }
  }

  private def setupSlots(): Unit = {
    eepromSlot = new EEPROMSlot(this)
    cpuSlot = new CPUSlot(this, computer.tier.min(Tier.Three))

    floppySlot = if (computer.tier >= Tier.Three) Some(new FloppySlot(computer)) else None

    memorySlots = Array(
      new MemorySlot(this, computer.tier.min(Tier.Three)),
      new MemorySlot(this, computer.tier.min(Tier.Three)))

    diskSlots = computer.tier match {
      case Tier.One => Array(new DiskSlot(this, Tier.One))
      case Tier.Two => Array(new DiskSlot(this, Tier.Two), new DiskSlot(this, Tier.One))
      case Tier.Three => Array(new DiskSlot(this, Tier.Three), new DiskSlot(this, Tier.Two))
      case _ => Array(new DiskSlot(this, Tier.Three), new DiskSlot(this, Tier.Three))
    }

    cardSlots = computer.tier match {
      case Tier.One => Array(new CardSlot(this, Tier.One), new CardSlot(this, Tier.One))
      case Tier.Two => Array(new CardSlot(this, Tier.Two), new CardSlot(this, Tier.One))
      case Tier.Three => Array(new CardSlot(this, Tier.Three), new CardSlot(this, Tier.Two), new CardSlot(this, Tier.Two))
      case _ => Array(new CardSlot(this, Tier.Three), new CardSlot(this, Tier.Three), new CardSlot(this, Tier.Three))
    }
  }

  override def draw(g: Graphics): Unit = {
    super.draw(g)

    val isRunning = computer.machine.isRunning
    val hasErrored = computer.machine.lastError != null

    if (isRunning && !hasErrored)
      g.sprite("nodes/ComputerOnOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)

    if (!isRunning && hasErrored)
      g.sprite("nodes/ComputerErrorOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)

    if (isRunning && System.currentTimeMillis() - computer.machine.lastDiskAccess < 400 && Math.random() > 0.1)
      g.sprite("nodes/ComputerActivityOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)
  }

  private var currentWindow: ComputerWindow = _

  override def window: Option[ComputerWindow] = {
    if (currentWindow == null) {
      currentWindow = new ComputerWindow(this)
      Some(currentWindow)
    } else Some(currentWindow)
  }
}
