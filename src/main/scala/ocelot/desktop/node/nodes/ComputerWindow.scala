package ocelot.desktop.node.nodes

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.{Padding2D, Size2D, Vector2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.layout.{Layout, LinearLayout}
import ocelot.desktop.ui.widget._
import ocelot.desktop.ui.widget.window.BasicWindow
import ocelot.desktop.util.{DrawUtils, Orientation}
import totoro.ocelot.brain.entity.Case
import totoro.ocelot.brain.util.DyeColor

class ComputerWindow(computerNode: ComputerNode) extends BasicWindow {
  def computer: Case = computerNode.computer

  def updateSlots(): Unit = {
    eepromBox.children(0) = computerNode.eepromSlot
    inner.children(1).children(1) = slotsWidget
  }

  private val eepromBox = new PaddingBox(computerNode.eepromSlot, Padding2D(right = 10))

  private val inner = new Widget {
    override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)

    children :+= new PaddingBox(new Label {
      override def text: String = computer.node.address
      override def isSmall: Boolean = true
      override def color: Color = ColorScheme("ComputerAddress")
    }, Padding2D(bottom = 8))

    children :+= new Widget {
      children :+= new PaddingBox(new Widget {
        children :+= new PaddingBox(computerNode.eepromSlot, Padding2D(right = 10))

        children :+= new IconButton("buttons/PowerOff", "buttons/PowerOn",
          isSwitch = true, sizeMultiplier = 2)
        {
          override def isOn: Boolean = computer.machine.isRunning
          override def onPressed(): Unit = computer.turnOn()
          override def onReleased(): Unit = computer.turnOff()
        }
      }, Padding2D(top = 44, left = 40))


      children :+= slotsWidget
    }
  }

  children :+= new PaddingBox(inner, Padding2D(10, 12, 10, 12))

  private def slotsWidget: Widget = {
    val rows = Array(
      computerNode.cardSlots,
      Array(computerNode.cpuSlot) ++ computerNode.memorySlots,
      computerNode.diskSlots ++ computerNode.floppySlot.toArray
    )

    val groot = root

    new Widget {
      override protected val layout = new Layout(this)

      root = groot

      for (row <- rows) {
        children :+= new Widget {
          override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
          for (slot <- row) children :+= slot
        }
      }

      override def minimumSize: Size2D = Size2D(158, 140)

      override def draw(g: Graphics): Unit = {
        children(0).position = position + Vector2D(19, 8)
        children(1).position = position + Vector2D(63, 8)
        children(2).position = position + Vector2D(107, 8)

        g.sprite("ComputerMotherboard", bounds.mapX(_ - 4).mapW(_ - 4))
        drawChildren(g)
      }
    }
  }

  override def draw(g: Graphics): Unit = {
    beginDraw(g)
    DrawUtils.windowWithShadow(g, position.x, position.y, size.width, size.height, 1f, 0.5f)
    drawChildren(g)
    endDraw(g)
  }
}