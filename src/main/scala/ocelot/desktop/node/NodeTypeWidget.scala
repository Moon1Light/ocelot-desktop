package ocelot.desktop.node

import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.event.handlers.{ClickHandler, HoverHandler}
import ocelot.desktop.ui.event.{ClickEvent, MouseEvent}
import ocelot.desktop.ui.widget.Widget
import ocelot.desktop.util.TierColor

class NodeTypeWidget(val nodeType: NodeType) extends Widget with ClickHandler with HoverHandler {
  override def minimumSize: Size2D = Size2D(68, 68)
  override def maximumSize: Size2D = minimumSize

  size = maximumSize

  def onClick(): Unit = {}

  override def receiveMouseEvents = true

  eventHandlers += {
    case ClickEvent(MouseEvent.Button.Left, _) => onClick()
  }

  override def draw(g: Graphics): Unit = {
    g.sprite(nodeType.icon, position.x + 2, position.y + 2, size.width - 4, size.height - 4, TierColor.get(nodeType.tier))
  }

  override def update(): Unit = {
    super.update()
    if (isHovered) {
      root.get.statusBar.addMouseEntry("icons/LMB", "Add node")
    }
  }
}
