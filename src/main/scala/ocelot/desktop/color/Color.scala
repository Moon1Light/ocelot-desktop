package ocelot.desktop.color

object Color {
  val White: RGBAColorNorm = RGBAColorNorm(1, 1, 1)
  val Transparent: RGBAColorNorm = RGBAColorNorm(0, 0, 0, 0)
}

trait Color {
  def toInt: IntColor

  def toRGBA: RGBAColor

  def toRGBANorm: RGBAColorNorm

  def toHSVA: HSVAColor
}
