package ocelot.desktop.color

case class RGBAColorNorm(r: Float, g: Float, b: Float, a: Float = 1f) extends Color {
  assert(0 <= r && r <= 1.0f, "Invalid RED channel")
  assert(0 <= g && g <= 1.0f, "Invalid GREEN channel")
  assert(0 <= b && b <= 1.0f, "Invalid BLUE channel")
  assert(0 <= a && a <= 1.0f, "Invalid ALPHA channel")

  def components: Array[Float] = Array(r, g, b, a)

  def mapA(f: Float => Float): RGBAColorNorm = copy(a = f(a))

  override def toInt: IntColor = toRGBA.toInt

  override def toRGBA: RGBAColor = RGBAColor((r * 255).toShort, (g * 255).toShort, (b * 255).toShort, (a * 255).toShort)

  override def toRGBANorm: RGBAColorNorm = this

  override def toHSVA: HSVAColor = {
    val max = r.max(g).max(b)
    val min = r.min(g).min(b)

    var hue = if (max == min)
      0
    else if (max == r)
      60 * (0 + (g - b) / (max - min))
    else if (max == g)
      60 * (2 + (b - r) / (max - min))
    else
      60 * (4 + (r - g) / (max - min))

    if (hue < 0) hue += 360

    val saturation = if (max == min)
      0
    else
      (max - min) / max

    val value = max

    HSVAColor(hue, saturation, value, a)
  }

  def withAlpha(alpha: Float): RGBAColorNorm = RGBAColorNorm(r, g, b, alpha)
}
