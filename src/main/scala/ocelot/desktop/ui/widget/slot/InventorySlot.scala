package ocelot.desktop.ui.widget.slot

import totoro.ocelot.brain.entity.traits.{Entity, Inventory}

abstract class InventorySlot[T <: Entity](inventory: Inventory) extends SlotWidget[T] {
  override def onAdded(item: T): Unit = {
    inventory.add(item)
  }

  override def onRemoved(item: T): Unit = {
    inventory.remove(item)
  }
}
