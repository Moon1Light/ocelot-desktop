package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconDef
import ocelot.desktop.node.nodes.ComputerNode
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import totoro.ocelot.brain.entity.Memory
import totoro.ocelot.brain.util.Tier

class MemorySlot(node: ComputerNode, val tier: Int) extends InventorySlot[Memory](node.computer) {
  override def itemIcon: Option[IconDef] = item.map(mem => new IconDef("items/Memory" + mem.tier))

  override def icon: IconDef = new IconDef("icons/Memory")
  override def tierIcon: Option[IconDef] = Some(new IconDef("icons/Tier" + tier))

  override def fillLmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(new ContextMenuEntry("RAM (Tier 1)",
      () => item = new Memory(Tier.One),
      icon = Some(new IconDef("items/Memory0"))))

    menu.addEntry(new ContextMenuEntry("RAM (Tier 1.5)",
      () => item = new Memory(Tier.Two),
      icon = Some(new IconDef("items/Memory1"))))

    if (tier >= Tier.Two) {
      menu.addEntry(new ContextMenuEntry("RAM (Tier 2)",
        () => item = new Memory(Tier.Three),
        icon = Some(new IconDef("items/Memory2"))))

      menu.addEntry(new ContextMenuEntry("RAM (Tier 2.5)",
        () => item = new Memory(Tier.Four),
        icon = Some(new IconDef("items/Memory3"))))
    }

    if (tier >= Tier.Three) {
      menu.addEntry(new ContextMenuEntry("RAM (Tier 3)",
        () => item = new Memory(Tier.Five),
        icon = Some(new IconDef("items/Memory4"))))

      menu.addEntry(new ContextMenuEntry("RAM (Tier 3.5)",
        () => item = new Memory(Tier.Six),
        icon = Some(new IconDef("items/Memory5"))))
    }
  }

  override def lmbMenuEnabled: Boolean = true
}
