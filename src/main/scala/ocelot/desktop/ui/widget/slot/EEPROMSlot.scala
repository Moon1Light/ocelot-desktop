package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconDef
import ocelot.desktop.node.nodes.ComputerNode
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import totoro.ocelot.brain.entity.EEPROM
import totoro.ocelot.brain.entity.traits.Entity
import totoro.ocelot.brain.loot.Loot

class EEPROMSlot(node: ComputerNode) extends InventorySlot[Entity](node.computer) {
  override def itemIcon: Option[IconDef] = item.map(_ => new IconDef("items/EEPROM"))
  override def icon: IconDef = new IconDef("icons/EEPROM")

  override def fillLmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(new ContextMenuEntry("Lua BIOS",
      () => item = Loot.OpenOsEEPROM.create(),
      icon = Some(new IconDef("items/EEPROM"))))
    menu.addEntry(new ContextMenuEntry("AdvLoader",
      () => item = Loot.AdvLoaderEEPROM.create(),
      icon = Some(new IconDef("items/EEPROM"))))
    menu.addEntry(new ContextMenuEntry("Empty",
      () => item = new EEPROM,
      icon = Some(new IconDef("items/EEPROM"))))

  }

  override def lmbMenuEnabled: Boolean = true
}
