package ocelot.desktop.ui.widget

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics

class Label extends Widget {
  def text: String = ""

  def isSmall: Boolean = false

  def color: Color = ColorScheme("Label")

  private var length = text.length * 8
  private def wideLength(g: Graphics): Int = text.map(g.font.charWidth(_)).sum

  override def minimumSize: Size2D = Size2D(length, if (isSmall) 8 else 16)
  override def maximumSize: Size2D = minimumSize.copy(width = Float.PositiveInfinity)

  override def draw(g: Graphics): Unit = {
    if (isSmall) g.setSmallFont()

    if (length < wideLength(g)) {
      length = wideLength(g)
      relayoutParent()
    }

    g.background = Color.Transparent
    g.foreground = color
    g.text(position.x, position.y, text)

    g.setNormalFont()
  }
}
