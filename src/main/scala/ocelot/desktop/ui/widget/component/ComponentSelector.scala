package ocelot.desktop.ui.widget.component

import ocelot.desktop.ColorScheme
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.Widget
import ocelot.desktop.util.animation.ValueAnimation
import ocelot.desktop.util.animation.easing.{EaseInOutQuad, EaseOutQuad}
import ocelot.desktop.util.{DrawUtils, Orientation}

class ComponentSelector extends Widget {
  override protected val layout = new LinearLayout(this, orientation = Orientation.Vertical)

  private var isClosing = false
  private var isOpening = false
  private val alpha = new ValueAnimation(0f, 8f)

  def isClosed: Boolean = {
    alpha.isAt(0f)
  }

  def open(): Unit = {
    isClosing = false
    isOpening = true
    alpha.jump(0.001f)
    alpha.goto(1f)
    alpha.easing = EaseInOutQuad
  }

  def close(): Unit = {
    isClosing = true
    isOpening = false
    alpha.goto(0f)
    alpha.easing = EaseOutQuad
  }

  override def draw(g: Graphics): Unit = {
    alpha.update()
    if (alpha.value < 1f) g.beginGroupAlpha() else isOpening = false

    val height = bounds.h * alpha.value

    DrawUtils.shadow(g, bounds.x - 8, bounds.y - 8, bounds.w + 16, height + 20, 0.5f)

    g.setScissor(bounds.x, bounds.y, bounds.w, height)
    g.rect(bounds, ColorScheme("ComponentSelectorBackground"))
    drawChildren(g)
    g.setScissor()

    DrawUtils.ring(g, bounds.x, bounds.y, bounds.w, height, 1, ColorScheme("ComponentSelectorBorder"))

    if (alpha.value < 1f) g.endGroupAlpha(alpha.value)
  }
}
