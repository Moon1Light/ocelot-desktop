package ocelot.desktop.ui.widget.card

import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.layout.{AlignItems, Layout, LinearLayout}
import ocelot.desktop.ui.widget.{Knob, Label, PaddingBox, Widget}
import ocelot.desktop.ui.widget.window.BasicWindow
import ocelot.desktop.util.{DrawUtils, Orientation}
import totoro.ocelot.brain.entity.Redstone
import totoro.ocelot.brain.util.DyeColor

class Redstone1Window(card: Redstone.Tier1) extends BasicWindow {
  private def redstoneKnob(side: Int) = new Knob(DyeColor.RED) {
    override def input: Int = {
      card.redstoneInput(side) min 15 max 0
    }

    override def input_=(v: Int): Unit = {
      card.redstoneInput(side) = v
    }

    override def output: Int = card.redstoneOutput(side) min 15 max 0
  }

  private def redstoneBlock(side: Int, name: String) = new PaddingBox(new Widget {
    override protected val layout: Layout = new LinearLayout(this, alignItems = AlignItems.Center)

    children :+= new PaddingBox(new Label {
      override def text: String = name
      override def maximumSize: Size2D = minimumSize
    }, Padding2D(right = 8))

    children :+= redstoneKnob(side)
  }, Padding2D.equal(4))

  children :+= new PaddingBox(new Widget {
    override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)

    children :+= new PaddingBox(new Label {
      override def text: String = s"Redstone I/O — ${card.node.address.substring(0, 14)}…"
      override val isSmall: Boolean = true
    }, Padding2D.equal(4))

    children :+= new Widget {
      children :+= new Widget {
        override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
        children :+= redstoneBlock(0, "Bottom")
        children :+= redstoneBlock(1, "   Top")
      }
      children :+= new Widget {
        override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
        children :+= redstoneBlock(2, " Back")
        children :+= redstoneBlock(3, "Front")
      }
      children :+= new Widget {
        override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
        children :+= redstoneBlock(4, "Right")
        children :+= redstoneBlock(5, " Left")
      }
    }
  }, Padding2D.equal(8))

  override def draw(g: Graphics): Unit = {
    beginDraw(g)
    DrawUtils.windowWithShadow(g, position.x, position.y, size.width, size.height, 1f, 0.5f)
    drawChildren(g)
    endDraw(g)
  }
}
