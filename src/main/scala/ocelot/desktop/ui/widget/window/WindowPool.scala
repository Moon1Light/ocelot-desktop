package ocelot.desktop.ui.widget.window

import ocelot.desktop.ui.event.handlers.DragHandler
import ocelot.desktop.ui.layout.Layout
import ocelot.desktop.ui.widget.Widget

class WindowPool extends Widget with DragHandler {
  override protected val layout: Layout = new Layout(this)

  private def windows: Array[Window] = children.map(_.asInstanceOf[Window])

  def addWindow(window: Window): Unit = {
    if (!children.contains(window)) {
      children :+= window
      if (window.isFocused) changeFocus(window)
    }
  }

  def openWindow(window: Window): Unit = {
    if (!children.contains(window)) {
      window.size = window.minimumSize
      val size = parent.get.size
      window.position = (size / 2 - window.size / 2).toVector.round
      window.show()
      children +:= window
      changeFocus(window)
    }
  }

  def closeWindow(window: Window): Unit = {
    val idx = windows.indexOf(window)
    children = children.patch(idx, Nil, 1)
    windows.lastOption.foreach(_.focus())
  }

  def closeAll(): Unit = {
    for (window <- windows) closeWindow(window)
  }

  def changeFocus(window: Window): Unit = {
    windows.lastOption.foreach(_.unfocus())
    window.focus()
    val idx = children.indexWhere(_ == window)
    children = children.patch(idx, Nil, 1) :+ window
  }
}
