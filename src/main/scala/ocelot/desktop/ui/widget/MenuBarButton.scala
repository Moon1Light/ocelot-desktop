package ocelot.desktop.ui.widget
import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.event.{ClickEvent, HoverEvent, MouseEvent}
import ocelot.desktop.ui.event.handlers.{ClickHandler, HoverHandler}
import ocelot.desktop.util.animation.ColorAnimation

class MenuBarButton(label: String, handler: () => Unit = () => {}) extends Widget with ClickHandler with HoverHandler {
  val color: ColorAnimation = new ColorAnimation(ColorScheme("TitleBarBackground"), 0.6f)

  children :+= new PaddingBox(new Label {
    override def text: String = label
    override def color: Color = ColorScheme("TitleBarButtonForeground")
    override def maximumSize: Size2D = minimumSize
  }, Padding2D(left = 8, right = 8, top = 1, bottom = 1))

  override def receiveMouseEvents: Boolean = true

  def onClick(): Unit = handler()

  def onMouseEnter(): Unit = color.goto(ColorScheme("TitleBarButtonActive"))

  def onMouseLeave(): Unit = color.goto(ColorScheme("TitleBarBackground"))

  eventHandlers += {
    case ClickEvent(MouseEvent.Button.Left, _) =>
      onClick()
    case HoverEvent(HoverEvent.State.Enter) =>
      onMouseEnter()
    case HoverEvent(HoverEvent.State.Leave) =>
      onMouseLeave()
  }

  override def draw(g: Graphics): Unit = {
    color.update()
    g.rect(bounds, color.getRGBANorm)
    drawChildren(g)
  }
}
