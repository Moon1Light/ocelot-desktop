package ocelot.desktop.ui.widget

import ocelot.desktop.{ColorScheme, OcelotDesktop}
import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.widget.contextmenu.ContextMenuEntry

class MenuBar extends Widget {
  override def receiveMouseEvents: Boolean = true

  private val entries = new Widget {}

  children :+= new PaddingBox(entries, Padding2D(left = 1, right = 1, bottom = 1))

  private def addEntry(w: Widget): Unit = entries.children :+= w

  addEntry(new MenuBarSubmenu("File", (menu) => {
    menu.addEntry(new ContextMenuEntry("New", () => OcelotDesktop.newWorkspace()))
    menu.addEntry(new ContextMenuEntry("Open", () => OcelotDesktop.open()))
    menu.addEntry(new ContextMenuEntry("Save", () => OcelotDesktop.save()))
    menu.addEntry(new ContextMenuEntry("Save as…", () => OcelotDesktop.saveAs()))
    menu.addSeparator()
    menu.addEntry(new ContextMenuEntry("Exit", () => OcelotDesktop.exit()))
  }))

  addEntry(new Widget {
    override def maximumSize: Size2D = Size2D(Float.PositiveInfinity, 1)
  }) // fill remaining space

  override def draw(g: Graphics): Unit = {
    g.rect(bounds, ColorScheme("TitleBarBackground"))
    drawChildren(g)
    g.rect(bounds.x, bounds.y + height - 1, width, height, ColorScheme("TitleBarBorder"))
  }
}
