package ocelot.desktop.ui.layout

object AlignItems extends Enumeration {
  val Start, End, Center, Stretch = Value
}
