package ocelot.desktop.ui.layout

import ocelot.desktop.geometry.Size2D
import ocelot.desktop.ui.widget.Widget

class Layout(widget: Widget) {
  protected var _minimumSize: Size2D = Size2D.Zero

  def minimumSize: Size2D = _minimumSize

  protected def minimumSize_=(value: Size2D): Unit = {
    _minimumSize = value
  }

  protected var _maximumSize: Size2D = Size2D.Inf

  def maximumSize: Size2D = _maximumSize

  protected def maximumSize_=(value: Size2D): Unit = {
    _maximumSize = value
  }

  def recalculateBounds(): Unit = {
    for (child <- widget.children)
      child.recalculateBounds()
  }

  def relayout(): Unit = {
    for (child <- widget.children)
      child.relayout()
  }
}
