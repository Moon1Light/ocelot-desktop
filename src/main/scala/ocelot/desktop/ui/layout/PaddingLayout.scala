package ocelot.desktop.ui.layout

import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.widget.Widget

class PaddingLayout(widget: Widget, padding: Padding2D) extends Layout(widget) {
  override def recalculateBounds(): Unit = {
    val inner = widget.children(0)
    minimumSize = inner.minimumSize + padding.sizeAddition
    maximumSize = inner.maximumSize + padding.sizeAddition
    widget.relayoutParent()
  }

  override def relayout(): Unit = {
    val inner = widget.children(0)
    inner.position = widget.position + padding.offset
    inner.size = widget.size - padding.sizeAddition
  }
}
