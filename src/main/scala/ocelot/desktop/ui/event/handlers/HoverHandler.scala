package ocelot.desktop.ui.event.handlers

import ocelot.desktop.ui.event.HoverEvent
import ocelot.desktop.ui.widget.Widget

trait HoverHandler extends Widget {
  private var wasHovered = false
  private var _isHovered: Boolean = false

  def isHovered: Boolean = _isHovered

  def setHovered(v: Boolean): Unit = {
    _isHovered = v

    if (_isHovered && !wasHovered)
      handleEvent(HoverEvent(HoverEvent.State.Enter))
    else if (!_isHovered && wasHovered)
      handleEvent(HoverEvent(HoverEvent.State.Leave))
    wasHovered = _isHovered
  }
}
