package ocelot.desktop.ui.event.sources

import ocelot.desktop.ui.event.KeyEvent
import ocelot.desktop.util.Logging
import org.lwjgl.input.Keyboard

import scala.collection.mutable

object KeyEvents extends Logging {
  private val _events = new mutable.ArrayBuffer[KeyEvent]
  private val pressedKeys = new mutable.HashMap[Int, Char]()

  def events: Iterator[KeyEvent] = _events.iterator

  def init(): Unit = {
    Keyboard.create()
    Keyboard.enableRepeatEvents(true)
  }

  def update(): Unit = {
    _events.clear()
    while (Keyboard.next()) {
      val code = Keyboard.getEventKey
      val char = Keyboard.getEventCharacter
      var state = if (Keyboard.getEventKeyState) KeyEvent.State.Press else KeyEvent.State.Release
      if (Keyboard.isRepeatEvent) state = KeyEvent.State.Repeat
      _events += KeyEvent(state, code, char)
      state match {
        case KeyEvent.State.Press | KeyEvent.State.Repeat =>
          pressedKeys += ((code, char))
        case KeyEvent.State.Release =>
          pressedKeys -= code
      }
    }
  }

  def destroy(): Unit = {
    Keyboard.destroy()
  }

  def isDown(code: Int): Boolean = pressedKeys.contains(code)

  def releaseKeys(): Unit = {
    for ((code, char) <- pressedKeys) {
      _events += KeyEvent(KeyEvent.State.Release, code, char)
    }

    pressedKeys.clear()
  }
}
